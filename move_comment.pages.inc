<?php

/**
 * @file
 * Contains the UI for the move_comment module.
 */

/**
 * Retrieves the form that lets the user decide what to do with the comment.
 * This is a form that modifies itself through AJAX based on the users input.
 */
function move_comment_get_action_form($cid) {
  $title = db_result(db_query('SELECT subject FROM {comments} WHERE cid = %d', $cid));
  if (!$title) {
    drupal_not_found();
    exit();
  }
  
  // Add the CSS that styles the comments tree
  drupal_add_css(drupal_get_path('module', 'move_comment') .'/move_comment_pages.css');
  // Load autocomplete.js. This doesn't happen automatically because the first form element
  // that needs it doesn't get rendered at load time, but only after the user has made a choice.
  drupal_add_js('misc/autocomplete.js');
  drupal_set_title(check_plain(t('Move comment') . ' "' . $title . '"'));
  $output = drupal_get_form('move_comment_action_form', $cid);
  return $output;
}

function move_comment_action_form(&$form_state, $cid) {
  $form = array();
  $parent_nid = db_result(db_query("SELECT nid FROM {comments} WHERE cid = %d", $cid));
  $parent_node = node_load($parent_nid);

  ahah_helper_register($form, $form_state);

  // For some reason, this has to be set manuall here for the form to work... ?
  $form['#submit'][] = 'move_comment_action_form_submit';

  $choice = $form_state['values']['options'];
  if (isset($form_state['values']['node_selector'])) {
    $nid_parts = explode(':', $form_state['values']['node_selector']);
    $nid = $nid_parts[0];
  }
  else {
    $nid = $parent_nid;
  }

  $form['cid'] = array('#type' => 'value', '#value' => $cid);

  $form['options'] = array(
    '#type' => 'radios',
    '#title' => t('Action'),
    '#options' => array(
      t('Move to another post'),
      t('Convert to a new topic')
    ),
    '#required' => TRUE,
    '#ahah' => array(
      'path' => ahah_helper_path(array('dynamic')),
      'wrapper' => 'move_form_container',
      'method' => 'replace',
      'effect' => 'fade'
    ),
  );

  $form['form_open'] = array('#value' => '<div id="move_form_container">');

  $form['dynamic'] = array();

  if (isset($choice) and ($choice == 0))
    $form['dynamic']['node_selector'] = _move_comment_node_selector_form($cid, $nid);

  $form['dynamic']['destination_form_open'] = array('#value' => '<div id="destination_form_container">');
  if (isset($choice) and $choice == 0)
    $form['dynamic']['destination'] = _move_comment_destination_form($nid);
  $form['dynamic']['destination_form_close'] = array('#value' => '</div>');


  if (isset($choice) && $choice == 1) {
    $title = $form_state['values']['title'];
    if (!$title)
      $title = db_result(db_query('SELECT subject FROM {comments} WHERE cid = %d', $cid));
    $form['dynamic']['title'] = _move_comment_title_form($title);

    $nodetype = $form_state['values']['node_type'];
    if (!$nodetype)
      $nodetype = $parent_node->type;
    $form['dynamic']['node_type'] = _move_comment_node_type_form($nodetype);
  }

  if (module_exists('taxonomy')) {
    $form['dynamic']['taxonomy_container_open'] = array('#value' => '<div id="taxonomy_container">');
    if (isset($choice) && $choice == 1) {
      $taxonomy = $form_state['values']['taxonomy'];
      if (!$taxonomy)
        $taxonomy = $parent_node->taxonomy;
      $form['dynamic']['taxonomy'] = _move_comment_taxonomy_form($nodetype, $taxonomy);
    }
    $form['dynamic']['taxonomy_container_close'] = array('#value' => '</div>');
  }


  if (isset($choice)) {
    $form['dynamic']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }


  $form['dynamic']['comment_tree_open'] = array('#value' => '<div id="comment_tree_container">');
  if (isset($choice) and $choice == 0) {
    $tree = theme('comment_tree', $nid, $cid);
    if (!$tree)
      $tree = t("Node not found. Please try again.");
    else
      $tree = t('This is a threaded list of the posts of the topic you selected. The post (and its descendants) that you are moving are shown in a darker color. If you hover over one of the posts, the first paragraph is shown in a tooltip. If you decide to move the comment to a certain post, you can click on it here (and then press the "!save" button).', array('!save' => t('Submit'))) . '<br/>' . $tree;
    $form['dynamic']['comment_tree'] = array('#value' => $tree);
  }
  $form['dynamic']['comment_tree_close'] = array('#value' => '</div>');


  $form['form_close'] = array('#value' => '</div>');

  return $form;
}

function _move_comment_title_form($default) {
  return array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of the new forum topic.'),
    '#default_value' => $default,
    '#required' => TRUE,
  );
}

function _move_comment_destination_form($nid) {
  return array(
    '#type' => 'textfield',
    '#title' => t('Move to'),
    '#prefix' => '<a name="destination-input-form"></a>',
    '#description' => t('The target post to which the comment should be moved.'),
    '#autocomplete_path' => 'move_comment_helper/destination_autocomplete/' . $nid,
    '#required' => TRUE,
  );
}

function _move_comment_taxonomy_form($type, $terms) {
  $form = array();

  $c = db_query(db_rewrite_sql("SELECT v.* FROM {vocabulary} v INNER JOIN {vocabulary_node_types} n ON v.vid = n.vid WHERE n.type = '%s' ORDER BY v.weight, v.name", 'v', 'vid'), $type);

  while ($vocabulary = db_fetch_object($c)) {
    if ($vocabulary->tags)
      continue;

    $default_terms = array();

    foreach ($terms as $term) {
      if (isset($term->vid) && $term->vid == $vocabulary->vid) {
        $default_terms[$term->tid] = $term;
      }
    }
    $form[$vocabulary->vid] = taxonomy_form($vocabulary->vid, array_keys($default_terms), filter_xss_admin($vocabulary->help));
    $form[$vocabulary->vid]['#weight'] = $vocabulary->weight;
    $form[$vocabulary->vid]['#required'] = $vocabulary->required;

    if ($type == 'forum' && $vocabulary->vid == variable_get('forum_nav_vocabulary', '')) {
      $form[$vocabulary->vid]['#required'] = TRUE;
      $form[$vocabulary->vid]['#options'][''] = t('- Please choose -');
    }
  }
  
  if (!empty($form) && is_array($form)) {
    if (count($form) > 1) {
      // Add fieldset only if form has more than 1 element.
      $form += array(
        '#type' => 'fieldset',
        '#title' => t('Vocabularies'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
    }
    $form['#tree'] = TRUE;
  }

  return $form;
}

function _move_comment_node_type_form($default) {
  $form = array();
  
  $types = array();
  $q = db_query('SELECT type,name FROM {node_type}');
  while ($type = db_fetch_object($q)) {
    $types[$type->type] = $type->name;
  }

  $form = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#default_value' => $default,
    '#options' => $types,
    '#required' => TRUE,
  );

  $form['#ahah'] = array(
    'path' => ahah_helper_path(array('dynamic', 'taxonomy')),
    'wrapper' => 'taxonomy_container',
    'method' => 'replace',
    'effect' => 'fade'
  );

  return $form;
}

function _move_comment_node_selector_form($cid, $nid) {
  $form = array();
  $node = node_load($nid);

  $form = array(
    '#type' => 'textfield',
    '#title' => t('The topic the post should be moved to'),
    '#description' => t('You can enter (a part of) its name or ID here.'),
    '#default_value' => $nid . ": " . $node->title,
    '#autocomplete_path' => 'move_comment_helper/node_autocomplete',
  );

  $form['#ahah'] = array(
    'path' => ahah_helper_path(array('dynamic', 'comment_tree')),
    'wrapper' => 'comment_tree_container',
    'method' => 'replace',
    'effect' => 'fade'
  );

  return $form;
}

/**
 * Autocomplete handler for the comment form field.
 */
function _move_comment_destination_autocomplete($nid, $str) {
  $matches = array();
  $node = node_load($nid);

  if (is_numeric($str)) {
    $comment_condition = 'nid = %d AND cid LIKE "%d%%"';
    if (strpos($node->nid, $str) === 0)
      $include_node = TRUE;
  }
  else {
    $comment_condition = 'nid = %d AND LOWER(subject) LIKE LOWER("%s%%")';
    if (stripos($node->title, $str) === 0)
      $include_node = TRUE;
  }
  $comments = db_query_range('SELECT cid,subject FROM {comments} WHERE ' . $comment_condition, $nid, $str, 0, 20);
  
  if ($include_node) {
    $str = "node:$nid: " . check_plain($node->title);
    $matches[$str] = $str;
  }
  while ($comment = db_fetch_object($comments)) {
    $str = "comment:" . $comment->cid . ": " . str_replace("\n", " ", str_replace("\r", " ", $comment->subject));
    $matches[$str] = check_plain($str);
  }
  
  print drupal_json($matches);
  exit();
}

/**
 * Autocomplete handler for the node form field.
 */
function _move_comment_node_autocomplete($str) {
  $matches = array();

  if (is_numeric($str)) {
    $query = db_rewrite_sql('SELECT n.nid,n.title FROM {node} n WHERE n.nid LIKE "%d%%"');
    $result = db_query_range($query, $str, 0, 20);
  }
  elseif (is_string($str)) {
    $query = db_rewrite_sql('SELECT n.nid,n.title FROM {node} n WHERE LOWER(n.title) LIKE LOWER("%s%%")');
    $result = db_query_range($query, $str, 0, 20);
  }

  while ($node = db_fetch_object($result)) {
    $str = $node->nid . ": " . $node->title;
    $matches[$str] = check_plain($str);
  }

  print drupal_json($matches);
  exit();
}

/**
 * Render the comment tree of the given node.
 *
 * @param $nid
 *   The node ID whose tree should be rendered.
 * @param $cid
 *   The post that is being moved.
 *
 * @return
 *   The rendered comment tree.
 *
 * @ingroup themeable
 */
function theme_comment_tree($nid, $cid) {
  if (!db_result(db_query('SELECT nid FROM {node} WHERE nid = %d', $nid)))
    return '';

  $node = node_load($nid);

  $comments = '<div id="comments-tree">';
  $divs = 0;
  $tree_divs = 0;
  $node_text = _move_comment_get_tooltip_text($node->body);

  $comments .= '<div class="comment-tree">';
  $comments .= theme('comment_tree_post', 'node', 'clickable', $nid, $node->name, $node->title, $node_text);
  $comments .= '<div class="children">';

  $result = db_query('SELECT c.cid, c.subject, c.thread, c.timestamp, c.name, c.uid, c.comment, u.name AS registered_name FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE nid = %d ORDER BY SUBSTRING(c.thread, 1, (LENGTH(c.thread) - 1))', $nid);

  // The following logic is based on that of comment_render from comment.module.
  while ($comment = db_fetch_object($result)) {
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    $comment->depth = count(explode('.', $comment->thread)) - 1;

    if ($comment->depth > $divs) {
      $divs++;
      $comments .= '<div class="children">';
    }
    else {
      while ($comment->depth < $divs) {
        $divs--;
        $comments .= '</div>';
      }
    }
    while ($comment->depth < $tree_divs) {
      $tree_divs--;
      $comments .= '</div>';
    }

    // Determine at what point we're at in the tree: the post which is to be moved
    // (mode 'post'), one of its descendants (mode 'descendant') or another post
    // (mode 'clickable').
    if ($comment->cid == $cid) { 
      // We're at the post. Set $post_level to be the current indentation level.
      // We use the fact that this variable is set as an indicator that we're in
      // the descendants tree.
      $mode = 'post';
      $post_level = $divs;
    }
    elseif (isset($post_level) && $divs > $post_level) {
      // We've encountered the post in rendering the previous comments, and this
      // is a descendant of the post
      $mode = 'descendant';
    }
    elseif (isset($post_level) && $divs <= $post_level) {
      // We've encountered the post in rendering the previous comments, but we're
      // no longer looking at one of its descendants.
      unset($post_level);
      $mode = 'clickable';
    }
    else {
      // We haven't encountered the post yet.
      $mode = 'clickable';
    }

    if ($mode == 'post') {
      $comments .= '<div class="comment-tree post-tree">';
    }
    else {
      $comments .= '<div class="comment-tree">';
    }
    $tree_divs++;
    
    $comments .= theme('comment_tree_post', 'comment', $mode, $comment->cid, $comment->name, $comment->subject, $comment->comment);
  }

  while ($divs-- > 0) {
    $comments .= '</div>';
  }
  while ($tree_divs-- > 0) {
    $comments .= '</div>';
  }

  return $comments . '</div></div></div>';
}

/**
 * Theme a post-header in the comment tree.
 *
 * @param $type
 *   The type of the post that is being renderd (node or comment).
 * @param $mode
 *   'clickable' if the post should be clickable to fill it in the destination box.
 * @param $id
 *   The ID of the post (nid or comment ID depending on $type).
 * @param $author
 *   The author of the post.
 * @param $subject
 *   The title of the post.
 * @param $text
 *   The text of the post.
 *
 * @return
 *   The themed post header.
 *
 * @ingroup themeable
 */
function theme_comment_tree_post($type, $mode, $id, $author, $subject, $text) {
  $author = check_plain($author);
  $subject = check_plain(str_replace("\n", " ", str_replace("\r", " ", $subject)));
  $title = "$id: $author &mdash; $subject";
  $text = _move_comment_get_tooltip_text($text);

  if ($mode == 'clickable') {
    $header = sprintf('<a href="#destination-input-form" onclick="$(\'#edit-destination\').val(\'%s\');">%s</a>', "$type:$id: $subject", $title);
  }
  else {
    $header = $title;
  }

  return "<div class=\"post-title\" title=\"$text\">$header</div>";
}

/**
 * Helper function that returns text suitable to show in a tooltip.
 *
 * It strips [quote]-tags and its contents, and returns the first non-empty line
 * of what remains of the argument, adding "(...)" if text was truncated.
 */
function _move_comment_get_tooltip_text($text) {
  // Strip quoted parts
  $text = preg_replace("|\[quote.*?\].*?\[/quote\]|s", '', strip_tags($text));
  
  // Grab the first line which is not empty
  $matches = array();
  preg_match('/^\s*(.*?)$/m', $text, $matches);
  $short_text = $matches[1];

  // Add dots when the text was shortened
  if ($text != $short_text)
    $short_text .= ' (...)';
  
  return $short_text;
}
